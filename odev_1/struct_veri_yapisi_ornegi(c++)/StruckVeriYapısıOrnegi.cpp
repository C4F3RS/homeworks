#include <iostream>
#include <conio.h>
using namespace std;

//struck veri yapisinda olacak degiskenleri tanimlama
struct telefon{
	char marka[50], model[50], islemci[50];
	int ram, depolama;
	float ekranBoyut;
};

int main(){
		//struck veri yapisindaki verileri kullanicidan alma
		struct telefon ozellik;
		printf("\nTelefonun ozelliklerini gir\n");
		cout<<"Marka: "<<endl;
		cin.getline(ozellik.marka, 50);
		cout<<"Model: "<<endl;
		cin.getline(ozellik.model, 50);
		cout<<"Islemci: "<<endl;
		cin.getline(ozellik.islemci, 50);
		cout<<"Ram(GB): "<<endl;
		cin>>ozellik.ram;
		cout<<"Dahili depolama(GB): "<<endl;
		cin>>ozellik.depolama;
		cout<<"Ekran boyutu(inc): "<<endl;
		cin>>ozellik.ekranBoyut;
		
		getch();
		system("cls");//terminal ekranini temizleme
		
		//struck yapisinaki verileri ekrana yazdirma
		cout<<"\nOzellikler kaydedildi.\n"<<endl;
		cout<<"Marka: "<<ozellik.marka<<endl;
		cout<<"Model: "<<ozellik.model<<endl;
		cout<<"Islemci: "<<ozellik.islemci<<endl;
		cout<<"Ram: "<<ozellik.ram<<" GB"<<endl;
		cout<<"Dahili depolama: "<<ozellik.depolama<<" GB"<<endl;
		cout<<"Ekran boyutu: "<<ozellik.ekranBoyut<<" inc\n"<<endl;
		
		getch();
		return 0;
}